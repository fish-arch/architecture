# Putting it together

For a modular system, we'd expect the following core modules to be present in the root module:

- Auth.
- Gateway.
- Monitor.
- Translation.
- DB Migrations.
- Reports.
- Deployer.

Optionally, it may also include:

- Cache (for shared cache between multiple nodes in HA installations).
- Tests.

The modules running over this should be able to register/unregister from the Gateway, report status to Monitor, and receive authentication data from Auth (as well be protected behind it).

## Auth

The Auth module must provide the capability to authenticate users or services, providing or denying access to the Gateway module.

It is desirable that it may be capable of logging and detecting routes/paths, to log blocked attempts, and specify security checks through the system.

## Gateway

The Gateway module takes care of submitting requests to the different modules installed.

Modules must self-register and reply to requests, after going through Auth.

High Availability/Load Balancing procedures may also be handled by it, through the received data from registry.

## Monitor

The Monitor module takes care of acknowledging if all entries in the gateway are up, as well listing times for requests, between other tasks.

## Translation

For translation support, a global Translation module must be present. This should provide the required strings to the modules for their operation with the user, as well acknowledge the "strings requested", in case there are missing.

## DB Migrations

This module may link up all modules and create tables/data for their operation, acknowledging the presence of each other for their "flexible" behaviour.

## Reports

This module may provide a reporting server or files related to it. May also link reports folders from other modules.

## Deployer

Finally, this is the maybe most important of all, the **Deployer** module performs installation and update tasks of the root module or submodules.