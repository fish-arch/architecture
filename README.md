# Fisherman Architecture Guidelines

## What is the Fisherman Architecture?

This architecture intends to establish a pattern for a **modular** source code management.

Due to it's nature, it's compatible with a **microservices** based architecture.

The main and most basic pattern is **api/app/dep/mig**, however, it can be only a single component, or may be enhanced with additional components such as **docs/tests/db/etc.**

## How does it compare to Docker/GitLab CI/CD?

**It cannot be and should NOT be compared to these technologies.**

Although this architecture has an **strong emphasis** on providing **guidelines for DevOps** , it can be combined with any other existing technology.

### Example

Imagine we have an BookStore software/module. It has an Angular frontend, an Node JS backend, Sequelize.JS database migrations and maybe some installer scripts.

There we already have our four basic components.
Our directory tree will have the following:

**/** -> BookStore project root
- **/api** -> NodeJS Server.
- **/app** -> Angular UI.
- **/mig** -> Sequelize JS migrations.
- **/dep** -> Deployment scripts.

As such, all projects have this practical separation.

Further details are specified separately in their own sections, those are:



## TODO:

- Add examples apps.
- Add example **root module** (the module containing the Ocelot Auth/Gateway, which also should link all the **mig** components to build global migrations, as well combine all app/api modules if their technologies allow it, for example, Angular root and modules).